-- wibar.lua
-- Wibar (top bar)
local awful = require("awful")
local gears = require("gears")
local gfs = require("gears.filesystem")
local wibox = require("wibox")
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local helpers = require("helpers")

local systray_margin = (beautiful.wibar_height - beautiful.systray_icon_size) / 2

-- Helper function that changes the appearance of progress bars and their icons
-- Create horizontal rounded bars
local function format_progress_bar(bar)
    bar.forced_width = dpi(100)
    bar.shape = helpers.rrect(beautiful.border_radius - 3)
    bar.bar_shape = helpers.rrect(beautiful.border_radius - 3)
    bar.background_color = beautiful.xcolor0
    return bar
end

-- Awesome Panel -----------------------------------------------------------

--[[ local unclicked = gears.surface.load_uncached(
                      gfs.get_configuration_dir() .. "icons/ghosts/awesome.png")

local clicked = gears.color.recolor_image(
                    gears.surface.load_uncached(
                        gfs.get_configuration_dir() ..
                            "icons/ghosts/awesome.png"), beautiful.xcolor8)

                            --]]
local icon1 =
    wibox.widget {
    widget = wibox.widget.imagebox,
    image = gears.surface.load_uncached(gfs.get_configuration_dir() .. "icons/ghosts/awesome.png"),
    resize = true
}

local awesome_icon =
    wibox.widget {
    {
        icon1,
        top = dpi(5),
        bottom = dpi(5),
        left = dpi(10),
        right = dpi(5),
        widget = wibox.container.margin
    },
    bg = beautiful.xcolor0,
    widget = wibox.container.background
}

awesome_icon:buttons(
    gears.table.join(
        awful.button(
            {},
            1,
            function()
                awesome.emit_signal("widgets::start::show", mouse.screen)
            end
        )
    )
)

--[[ awesome.connect_signal("widgets::start::status", function(status)
    if not status then
        icon1.image = unclicked
    else
        icon1.image = clicked
    end
end)
--]]
-- Notifs Panel ---------------------------------------------------------------

local icon2 =
    wibox.widget {
    widget = wibox.widget.textbox,
    font = beautiful.icon_font,
    markup = "<span foreground='" .. beautiful.xcolor4 .. "'>" .. "" .. "</span>",
    resize = true
}

local notif_icon =
    wibox.widget {
    {
        icon2,
        top = dpi(4),
        right = dpi(7),
        left = dpi(7),
        bottom = dpi(4),
        widget = wibox.container.margin
    },
    bg = beautiful.xcolor0,
    widget = wibox.container.background
}

notif_icon:buttons(
    gears.table.join(
        awful.button(
            {},
            1,
            function()
                awesome.emit_signal("widgets::notif_panel::show", mouse.screen)
            end
        )
    )
)

--[[awesome.connect_signal("widgets::notif_panel::status", function(status)
    if not status then
        icon2.markup = "<span foreground='" .. beautiful.xcolor4 .. "'>" ..
                           "" .. "</span>"
    else
        icon2.markup = "<span foreground='" .. beautiful.xcolor8 .. "'>" ..
                           "" .. "</span>"

    end
end)--]]
-- Systray Widget -------------------------------------------------------------

local mysystray = wibox.widget.systray()
mysystray:set_base_size(beautiful.systray_icon_size)

local mysystray_container = {
    mysystray,
    left = dpi(8),
    right = dpi(8),
    widget = wibox.container.margin
}

-- Tasklist Buttons -----------------------------------------------------------

local tasklist_buttons =
    gears.table.join(
    awful.button(
        {},
        1,
        function(c)
            if c == client.focus then
                c.minimized = true
            else
                c:emit_signal("request::activate", "tasklist", {raise = true})
            end
        end
    ),
    awful.button(
        {},
        3,
        function()
            awful.menu.client_list({theme = {width = 250}})
        end
    ),
    awful.button(
        {},
        4,
        function()
            awful.client.focus.byidx(1)
        end
    ),
    awful.button(
        {},
        5,
        function()
            awful.client.focus.byidx(-1)
        end
    )
)

-- Playerctl Bar Widget -------------------------------------------------------

-- Title Widget
local song_title =
    wibox.widget {
    markup = "Nothing Playing",
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox
}

local song_artist =
    wibox.widget {
    markup = "nothing playing",
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox
}

local song_logo =
    wibox.widget {
    markup = '<span foreground="' .. beautiful.xcolor6 .. '"></span>',
    font = beautiful.icon_font,
    align = "center",
    valign = "center",
    widget = wibox.widget.textbox
}

local playerctl_bar =
    wibox.widget {
    {
        {
            {
                song_logo,
                left = dpi(3),
                right = dpi(10),
                bottom = dpi(1),
                widget = wibox.container.margin
            },
            {
                {
                    song_title,
                    expand = "outside",
                    layout = wibox.layout.align.vertical
                },
                left = dpi(10),
                right = dpi(10),
                widget = wibox.container.margin
            },
            {
                {
                    song_artist,
                    expand = "outside",
                    layout = wibox.layout.align.vertical
                },
                left = dpi(10),
                widget = wibox.container.margin
            },
            spacing = 1,
            spacing_widget = {
                bg = beautiful.xcolor8,
                widget = wibox.container.background
            },
            layout = wibox.layout.fixed.horizontal
        },
        left = dpi(10),
        right = dpi(10),
        widget = wibox.container.margin
    },
    bg = beautiful.xcolor0,
    shape = helpers.rrect(beautiful.border_radius - 3),
    widget = wibox.container.background
}

playerctl_bar.visible = false

awesome.connect_signal(
    "bling::playerctl::player_stopped",
    function()
        playerctl_bar.visible = false
    end
)

-- Get Title
awesome.connect_signal(
    "bling::playerctl::title_artist_album",
    function(title, artist, _)
        playerctl_bar.visible = true
        song_title.markup = '<span foreground="' .. beautiful.xcolor5 .. '">' .. title .. "</span>"

        song_artist.markup = '<span foreground="' .. beautiful.xcolor4 .. '">' .. artist .. "</span>"
    end
)

-- Create the Wibar -----------------------------------------------------------

screen.connect_signal(
    "request::desktop_decoration",
    function(s)
        -- Create a promptbox for each screen
        s.mypromptbox = awful.widget.prompt()

        -- Create layoutbox widget
        s.mylayoutbox = awful.widget.layoutbox(s)

        -- Create the wibox
        s.mywibox = awful.wibar({position = "bottom", screen = s})

        -- Remove wibar on full screen
        local function remove_wibar(c)
            if c.fullscreen or c.maximized then
                c.screen.mywibox.visible = false
            else
                c.screen.mywibox.visible = true
            end
        end

        -- Remove wibar on full screen
        local function add_wibar(c)
            if c.fullscreen or c.maximized then
                c.screen.mywibox.visible = true
            end
        end

        -- Hide bar when a splash widget is visible
        awesome.connect_signal(
            "widgets::splash::visibility",
            function(vis)
                screen.primary.mywibox.visible = not vis
            end
        )

        client.connect_signal("property::fullscreen", remove_wibar)

        client.connect_signal("request::unmanage", add_wibar)

        -- Create the taglist widget
        s.mytaglist = require("bloat.widgets.pacman_taglist")(s)

        -- Create a tasklist widget
        s.mytasklist =
            awful.widget.tasklist {
            screen = s,
            filter = awful.widget.tasklist.filter.currenttags,
            buttons = tasklist_buttons,
            bg = beautiful.xcolor0,
            style = {bg = beautiful.xcolor0},
            layout = {
                spacing = dpi(0),
                spacing_widget = {
                    bg = beautiful.xcolor8,
                    widget = wibox.container.background
                },
                layout = wibox.layout.fixed.horizontal
            },
            widget_template = {
                {
                    {
                        nil,
                        awful.widget.clienticon,
                        nil,
                        layout = wibox.layout.fixed.horizontal
                    },
                    top = dpi(5),
                    bottom = dpi(5),
                    left = dpi(10),
                    right = dpi(10),
                    widget = wibox.container.margin
                },
                id = "background_role",
                widget = wibox.container.background
            }
        }

        local final_systray =
            wibox.widget {
            {mysystray_container, top = dpi(5), layout = wibox.container.margin},
            bg = beautiful.xcolor0,
            shape = helpers.rrect(beautiful.border_radius - 3),
            widget = wibox.container.background
        }

        -- Add widgets to the wibox
        s.mywibox:setup {
            layout = wibox.layout.fixed.vertical,
            {
                widget = wibox.container.background,
                bg = beautiful.xcolor0,
                forced_height = beautiful.widget_border_width
            },
            {
                layout = wibox.layout.align.horizontal,
                expand = "none",
                {
                    layout = wibox.layout.fixed.horizontal,
                    {
                        {
                            {
                                awesome_icon,
                                s.mytaglist,
                                spacing = 15,
                                spacing_widget = {
                                    color = beautiful.xcolor8,
                                    shape = gears.shape.powerline,
                                    widget = wibox.widget.separator
                                },
                                layout = wibox.layout.fixed.horizontal
                            },
                            bg = beautiful.xcolor0,
                            shape = helpers.rrect(beautiful.border_radius - 3),
                            widget = wibox.container.background
                        },
                        top = dpi(5),
                        left = dpi(10),
                        right = dpi(5),
                        bottom = dpi(5),
                        widget = wibox.container.margin
                    },
                    s.mypromptbox,
                    {
                        playerctl_bar,
                        margins = dpi(5),
                        widget = wibox.container.margin
                    }
                },
                {
                    {
                        {
                            s.mytasklist,
                            bg = beautiful.xcolor0 .. "00",
                            shape = helpers.rrect(beautiful.border_radius - 3),
                            widget = wibox.container.background
                        },
                        margins = dpi(5),
                        widget = wibox.container.margin
                    },
                    widget = wibox.container.constraint
                },
                {
                    nil,
                    nil,
                    nil,
                    {
                        awful.widget.only_on_screen(final_systray, screen[1]),
                        margins = dpi(5),
                        widget = wibox.container.margin
                    },
                    {
                        {
                            {
                                s.mylayoutbox,
                                top = dpi(4),
                                bottom = dpi(4),
                                right = dpi(7),
                                left = dpi(7),
                                widget = wibox.container.margin
                            },
                            bg = beautiful.xcolor0,
                            shape = helpers.rrect(beautiful.border_radius - 3),
                            widget = wibox.container.background
                        },
                        margins = dpi(5),
                        widget = wibox.container.margin
                    },
                    {
                        {
                            notif_icon,
                            bg = beautiful.xcolor0,
                            shape = helpers.rrect(beautiful.border_radius - 3),
                            widget = wibox.container.background
                        },
                        top = dpi(5),
                        right = dpi(10),
                        left = dpi(5),
                        bottom = dpi(5),
                        widget = wibox.container.margin
                    },
                    layout = wibox.layout.fixed.horizontal
                }
            }
        }
    end
)

-- EOF ------------------------------------------------------------------------
